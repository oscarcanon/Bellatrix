import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Exercice {
    public static void main(String[] args) {
    	System.setProperty("webdriver.gecko.driver","/home/oscarcanon/eclipse-workspace/Bellatrix/External/geckodriver");
    	WebDriver driver = new FirefoxDriver();
        String baseUrl = "http://www.ebay.com";
        String tagName = "";
        String tagItemPrice = "";
        
        driver.get(baseUrl);
        driver.findElement(By.xpath("//a[@id='gh-eb-Geo-a-default']/span[2]")).click();
        driver.findElement(By.xpath("//a[@id='gh-eb-Geo-a-en']/span[2]")).click();
        driver.findElement(By.id("gh-shop-a")).click();
        driver.findElement(By.linkText("Shoes")).click();
        driver.findElement(By.id("gh-ac")).click();
        driver.findElement(By.id("gh-ac")).clear();
        driver.findElement(By.id("gh-ac")).sendKeys("puma");
        driver.findElement(By.id("gh-btn")).click();
        driver.findElement(By.id("e1-29")).click();
        tagName = driver.findElement(By.xpath("//div[@id='cbelm']/div[3]/h1/span")).getText();
        System.out.println("The amount of results is: "+tagName);
        driver.findElement(By.linkText("Best Match")).click(); 
        WebElement element = driver.findElement(By.linkText("Price + Shipping: lowest first"));
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
        element.click(); 
                
        for (int i=1; i<=5; i++)
        {
        String tagItemName = driver.findElement(By.xpath("//ul[@id='GalleryViewInner']/li["+i+"]/div/div[2]/h3/a")).getText();
        //if (!(driver.findElement(By.xpath("//ul[@id='GalleryViewInner']/li["+i+"]/div/div[3]/div[2]/span"))==null))
        //{
        //	tagItemPrice = driver.findElement(By.xpath("//ul[@id='GalleryViewInner']/li["+i+"]/div/div[3]/div[2]/span")).getText();
        //}
        //else
        //{
        //	tagItemPrice = driver.findElement(By.xpath("//ul[@id='GalleryViewInner']/li["+i+"]/div/div[3]/div[2]/div/span[1]/span")).getText();
        //}
        
        System.out.println("The item in possition "+i+" is: "+tagItemName+ " and their price is: "+tagItemPrice);        
        }     
        driver.close();
        System.exit(0);
}
}